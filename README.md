# Hello IoT

IoT Examples for ESP32 devkits

## web_remote.cpp

This code uses the WiFi, AsyncTCP, and ESPAsyncWebServer libraries to serve a minimal web interface to control an LED over a lan network.
To use it you will have to add your network and password to line 15 before flashing your ESP32.
By default the program reports its dynamic IP over serial, however you can check or statically route it using your router.
