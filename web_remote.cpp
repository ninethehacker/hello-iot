#include <Arduino.h>
#include <WiFi.h>               //WiFi library
#include <AsyncTCP.h>           //Esp32 TCP stack
#include <ESPAsyncWebServer.h>  //Esp32 WebServer

int LED_BUILTIN = 2;
AsyncWebServer server(80);
char html[] = "<h1>Hello, ESP32</h1><br><a href=\"/lighton\">lighton</a> <a href=\"/lightoff\">lightoff</a>";

void setup() {
    Serial.begin(9600);
    pinMode (LED_BUILTIN, OUTPUT);
    Serial.println("Hello, ESP32 :D");

    WiFi.begin("<SSID>", "<PASSWORD>");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.println("Connecting WiFi");
    }
    Serial.println("Connected to WiFi");
    Serial.println("IP: " + String(WiFi.localIP())); //FIXME: wrong print
    Serial.println(WiFi.localIP());

    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", html);
        Serial.println("Recieved HTTP_GET");
    });

    server.on("/lighton", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", html);
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println("Recieved HTTP_GET (light on)");
    });

    server.on("/lightoff", HTTP_GET, [](AsyncWebServerRequest *request) {
        request->send(200, "text/html", html);
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("Recieved HTTP_GET (light off)");
    });

    server.begin();
    Serial.println("WebServer Online");
}

void loop() {}
